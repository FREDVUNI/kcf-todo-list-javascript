# Todo List App

This is a simple todo list app that allows users to add, edit, and remove items from a list. The app is built using HTML, CSS, and JavaScript.

## Features

- Add a new item to the to-do list by entering text in the input field and clicking the "Add" button or pressing the "Enter" key.
- Each list item has a remove button that removes it from the list when clicked.
- Each list item has an edit button that makes the item editable. The edit button changes to a save button when clicked, and the item can be saved by clicking the save button or pressing the "Enter" key. The date label shows when the list item was created if not yet edited, else when it was edited.

## Technologies Used

- HTML
- CSS
- JavaScript

## Deployment

This project is deployed on Netlify. You can access it by clicking on the following link: [https://kcf-javascript-todo.netlify.app/](https://kcf-javascript-todo.netlify.app/)

## How to Use

1. Clone the repository to your local machine.
2. Open the `index.html` file in your web browser.
3. Enter a new item in the input field and click the "Add" button or press the "Enter" key to add it to the list.
4. To remove an item from the list, click the "Remove" button next to the item.
5. To edit an item, click the "Edit" button next to the item. Edit the text in the input field and click the "Save" button or press the "Enter" key to save the changes.

## Credits

This project was created by FRED VUNI
