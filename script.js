document.addEventListener("DOMContentLoaded", function () {
  const todoForm = document.getElementById("todoForm");
  const todoInput = document.getElementById("todoInput");
  const todoList = document.getElementById("todoList");

  todoForm.addEventListener("submit", function (event) {
    event.preventDefault();
    const newTodo = todoInput.value;
    const date = new Date().toLocaleDateString();
    const newTodoItem = document.createElement("li");
    newTodoItem.classList.add("list-group-item");
    newTodoItem.innerHTML =
      '<div class="form-check"><input type="checkbox" class="form-check-input"/><label class="check-label">' +
      newTodo +
      '</label></div><div class="date">' +
      "created: " +
      date +
      '</div><div class="button-container"><i class="fas fa-pencil-alt edit-button"></i><i class="remove-button fas fa-trash"></i></div>';
    todoList.appendChild(newTodoItem);
    todoInput.value = "";
  });

  todoList.addEventListener("click", function (event) {
    if (event.target.classList.contains("remove-button")) {
      const todoItem = event.target.closest("li");
      todoList.removeChild(todoItem);
    } else if (event.target.classList.contains("edit-button")) {
      const label = event.target.closest("li").querySelector(".check-label");
      const date = event.target.closest("li").querySelector(".date");
      const text = label.textContent.trim();
      label.innerHTML =
        '<input type="text" class="edit-input" value="' + text + '"/>';
      date.textContent = "Edited: " + new Date().toLocaleString();
    }
  });

  document
    .getElementById("todoList")
    .addEventListener("keypress", function (event) {
      if (event.target.classList.contains("edit-input") && event.which === 13) {
        let newText = event.target.value.trim();
        let label = document.createElement("label");
        label.classList.add("check-label");
        label.textContent = newText;
        event.target.parentNode.replaceChild(label, event.target);
        let date = event.target.closest("li").querySelector(".date");
        date.textContent = "Edited: " + new Date().toLocaleString();
      }
    });
});
